#include "../include/FileSettingsLoader.h"
#include <cstdio>
#include <fstream>
#include <gtest/gtest.h>
#include <iostream>
struct A
{
	int _e = 0;
	std::string _f;
	inline void setE( const std::string& newA )
	{
		_e = std::atoi( newA.c_str() );
	}
	inline void setF( const std::string& newF )
	{
		_f = newF;
	}
};
namespace X
{
TEST( fileSettingsLoader, addProperties )
{
	FileSettingsLoader loader( "test.conf" );
	A a;
	using std::placeholders::_1;
	std::function< void( const std::string& ) > setterF = std::bind( &A::setF, &a, _1 );
	loader.addProperties( "F", setterF );
	EXPECT_EQ( loader._properties.size(), 1 ) << "incorrect properties count";
	loader._properties["F"]( "newF" );
	EXPECT_EQ( a._f, "newF" ) << "property not set";
}
TEST( fileSettingsLoader, load )
{
	//create file
	std::string fileName = "file.conf";
	std::fstream file;
	file.open( fileName, std::ios_base::out );
	EXPECT_TRUE( file.is_open() ) << "file not open";
	//save settings
	file << "E"
		 << " " << 4 << "\n";
	file << "F"
		 << " "
		 << "newF"
		 << "\n";
	file.close();
	//create loader
	FileSettingsLoader loader( fileName );
	loader.setVerbose( true );
	//create object which properties will be loaded
	A a;
	using std::placeholders::_1;
	std::function< void( const std::string& ) > setterE = std::bind( &A::setE, &a, _1 );
	loader.addProperties( "E", setterE );
	loader.addProperties< A >( "F", &a, &A::setF );
	std::function< void( const std::string& ) > setterF = std::bind( &A::setF, &a, _1 );
	loader.addProperties( "F", setterF );
	bool out = loader.load();
	//compare loaded value witch references
	EXPECT_TRUE( out ) << "load reaturn false";
	EXPECT_EQ( a._e, 4 ) << "incorrect _e value";
	EXPECT_EQ( a._f, "newF" ) << "incorrect _f value";
}
} // namespace X
