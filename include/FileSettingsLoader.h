#ifndef FILESETTINGSLOADER
#define FILESETTINGSLOADER
#include <functional>
#include <gtest/gtest_prod.h>
#include <map>
#include <string>
namespace X
{
/**
 *
 * @brief The FileSettingsLoader class is simple class to load settings from
 * file. To use this class You must provide public setters for your properties.
 * Setters must take only one arguemnt of type std::string.
 * @code
 * stuct MyStruct
 * {
 *     int _property;
 *     void setProperty( const std::string& value)
 *     {
 *         _property = std::atoi( value );
 *     }
 * }
 * @endcode
 * After construction user must determine which properties will be loaded.
 * Properties names must be same as names in config file.
 * @code
 * MyStruct my;
 * FileSettingsLoader loader("/opt/my_program/config_file");
 * using std::placeholders::_1;
 * std::function<void(const std::string&)> setter = std::bind(
 * &MyStruct::setProperty, &my , _1) ; loader.setProperty ("PROPERTY", setter );
 * loader.load();
 * @endcode
 */
class FileSettingsLoader
{
	FRIEND_TEST( fileSettingsLoader, addProperties );
	FRIEND_TEST( fileSettingsLoader, load );

public:
    /**
   * @brief Constructs FileSettingsLoader object. Argument passed to function
   * sets class path, from which settings will be loaded.
   * @param path - path to settings file
   * @sa setPath(const std::string& path)
   */
    explicit FileSettingsLoader( const std::string& path = "file.conf" );
    /**
   * @brief Getter to class member path. From this path settings will be loaded.
   * @return - return path to config file.
   */
    std::string path() const;
    /**
   * @brief Setter to member "path". From this path settings will be laaded.
   * @param path - path to config file.
   */
    void setPath( const std::string& path );
    /**
   * @brief Function loads settings from file.
   * @return
   */
    bool load();
    /**
   * @brief Function add properties which would be loaded. Passed arguments
   * determinate property name and function, witch will be called when that name
   * will be found in config file. All passed function must have a signature
   * with one and only one argument with std::string type.
   * @param name - property name to find in config file
   * @param setter - function which must be called  when this property will be
   * found.
   */
    void addProperties( const std::string& name, std::function< void( const std::string& ) > setter );
    /**
   *@brief Overloaded function addProperties.
   */
    template < typename O, typename F > void addProperties( const std::string& name, O* object, F ( O::*function )( const std::string& ) )
    {
        using std::placeholders::_1;
        std::function< void( const std::string& ) > setter = std::bind( function, object, _1 );
        _properties.emplace( name, setter );
    }
    /**
   * @brief Helper function to trim string.
   * @param value - sring to trim
   * @return - trimmed string
   */
    std::string trim( std::string value ) const;
    /**
   * @brief This function return value of class member with the same name. If
   * it's set to true then library more logging to stdout.
   * @return
   */
    bool verbose() const;
    /**
   * @brief Function to setup  class member value.
   * @param verbose
   */
    void setVerbose( bool verbose );

private:
	std::string _path;
	std::map< std::string, std::function< void( const std::string& ) > > _properties;
	bool _verbose;
	/**
   * @brief Helper function to log message to stndard out;
   * @param messge
   */
    void log( const std::string& message );
    /**
   * @brief Helper funciton, to log error to stderr.
   * @param error
   */
    void error( const std::string& error );
};
} // namespace X
#endif // FILESETTINGSLOADER
