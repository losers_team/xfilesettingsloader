#include "FileSettingsLoader.h"
#include <fstream>
#include <iostream>
#include <regex>
X::FileSettingsLoader::FileSettingsLoader( const std::string& path )
    : _path( path )
    , _verbose( false )
{
}

std::string X::FileSettingsLoader::path() const
{
	return _path;
}

void X::FileSettingsLoader::setPath( const std::string& path )
{
	_path = path;
}

bool X::FileSettingsLoader::load()
{
	log( "load function start" );
	bool out = false;
	std::ifstream file( _path, std::ios_base::in );
	if ( file.is_open() )
	{
		log( "file " + _path + " opened" );
		// make copy of properties, check single property only once
		auto properties = _properties;
		std::string line;
		std::regex property( "" );
		std::string value;
		while ( std::getline( file, line ) )
		{
			for ( auto i = properties.begin(); i != properties.end(); ++i )
			{
				std::string name = i->first;
				property.assign( "\\b" + name + "\\b" );
				if ( std::regex_search( line, property ) )
				{
					value = trim( std::regex_replace( line, property, "" ) );
					properties[name]( value );
					properties.erase( name );
					log( "found property " + name + " with value: " + value );
					break;
				}
			}
		}
		out = true;
	}
	else
	{
	}

	return out;
}

void X::FileSettingsLoader::addProperties( const std::string& name, std::function< void( const std::string& ) > setter )
{
	_properties.emplace( name, setter );
}

std::string X::FileSettingsLoader::trim( std::string value ) const
{
	std::string chars = "\t\n\v\f\r ";
	std::string out;
	// rtrim
	out = value.erase( 0, value.find_first_not_of( chars ) );
	// ltrim
	out = out.erase( out.find_last_not_of( chars ) + 1 );
	return out;
}

bool X::FileSettingsLoader::verbose() const
{
	return _verbose;
}

void X::FileSettingsLoader::setVerbose( bool verbose )
{
	_verbose = verbose;
}

void X::FileSettingsLoader::log( const std::string& message )
{
	if ( _verbose )
	{
		std::cout << message << "\n";
	}
}

void X::FileSettingsLoader::error( const std::string& error )
{
	if ( _verbose )
	{
		std::cerr << error << "\n";
	}
}
