#doxygen
option( BUILD_DOC "documentation build" ON )
if(${CMAKE_PROJECT_NAME} STREQUAL ${PROJECT_NAME} )
find_package( Doxygen )
if( DOXYGEN_FOUND)
	set( DOXYGEN_PROJECT_NAME ${PROJECT_NAME} )
	set( DOXYGEN_PROJECT_VERSION ${CMAKE_PROJECT_VERSION} )
	set( DOXYGEN_GENERATE_HTML YES )
	set( DOXYGEN_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/doc/)
	set( DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/build/CMakeDoxyfile.in)
	set( DOXYGEN_INPUT ${CMAKE_SOURCE_DIR}/include/)
	set( DOXYGEN_OUT ${CMAKE_SOURCE_DIR}/doc/Doxyfile )
	configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
	add_custom_target( ${PROJECT_NAME}+Doc ALL
		   COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
		   WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
		   COMMENT "Generating API documentation with Doxygen"
		   VERBATIM )
	doxygen_add_docs(doxygen ${PROJECT_SOURCE_DIR} COMMENT "documentation generated" )
else()
	message( "doxygen package not found" )
endif( DOXYGEN_FOUND )
endif()
